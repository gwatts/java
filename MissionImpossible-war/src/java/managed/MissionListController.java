/**
 * BE Java EE "Mission Impossible"
 * Mineure "développement web"
 * CentraleSupélec, campus de Rennes
 * 
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 * 2016
 */
package managed;

import java.util.ArrayList;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import missions.EtatMission;
import missions.ListeMissions;
import missions.Mission;
import missions.TraitementMissionsRemote;

/**
 * "Managed bean" pour accéder à la liste des missions.
 *
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 */
@ManagedBean(name="missionListController")
@SessionScoped
public class MissionListController {
    //TODO Il manquera très certainement des méthodes dans cette classe.
    
    /**
     * EJB encapsulant toutes les missions connues du système.
     */
    @EJB
    private ListeMissions missions;
    
    /**
     * EJB proposant l'API de traitement des missions.
     */
    @EJB
    private TraitementMissionsRemote tm;

    /**
     * Constructeur par défaut. 
     */
    public MissionListController() {
    }
    
    /**
     * Récupération de l'ensemble des missions.
     * @return Liste de toutes les missions connues du système.
     */
    public ArrayList<Mission> getMissions() {
        return this.missions.getMissions();
    }
    public ArrayList<Mission> getMissionsDemandees()
    {
        return this.missions.getMissionsDemandees();
    }
    
    
    public ArrayList<Mission> getMissionsParEtatSuiviesPar(String etat, String admin)
    {
        switch (etat) {
         case "ATTRIBUEE":
             return this.missions.getMissionsParEtatSuiviesPar(EtatMission.ATTRIBUEE, admin);
         case "REFUSEE":
             return this.missions.getMissionsParEtatSuiviesPar(EtatMission.REFUSEE, admin);
         case "VALIDEE":
             return this.missions.getMissionsParEtatSuiviesPar(EtatMission.VALIDEE, admin);
         case "EN_COURS":
             return this.missions.getMissionsParEtatSuiviesPar(EtatMission.EN_COURS, admin);
         case "PAYEE":
             return this.missions.getMissionsParEtatSuiviesPar(EtatMission.PAYEE, admin);
         default :
             return null;
        }
    }
    
    public ArrayList<Mission> getMissionsParEtatFaitesPar(String etat, String agent)
    {
        switch (etat) {
         case "DEMANDEE" :
             return this.missions.getMissionsParEtatFaitesPar(EtatMission.DEMANDEE, agent); 
         case "ATTRIBUEE":
             return this.missions.getMissionsParEtatFaitesPar(EtatMission.ATTRIBUEE, agent);
         case "REFUSEE":
             return this.missions.getMissionsParEtatFaitesPar(EtatMission.REFUSEE, agent);
         case "VALIDEE":
             return this.missions.getMissionsParEtatFaitesPar(EtatMission.VALIDEE, agent);
         case "EN_COURS":
             return this.missions.getMissionsParEtatFaitesPar(EtatMission.EN_COURS, agent);
         case "PAYEE":
             return this.missions.getMissionsParEtatFaitesPar(EtatMission.PAYEE, agent);
         default :
             return null;
        }
    }
    
    
    
    
    public void attribuer(Mission m, String admin)
    {
        m.setEtat(EtatMission.ATTRIBUEE);
        m.setAgentAdmin(admin);
    }
    
    public void valider(Mission m)
    {
        m.setEtat(EtatMission.VALIDEE);
    }
    
    public void payer(Mission m)
    {
        m.setEtat(EtatMission.PAYEE);
    }
    
    public void refuser(Mission m)
    {
        m.setEtat(EtatMission.REFUSEE);
    }
    
    /**
     * Introduction d'une nouvelle mission dans le système.
     * @param m La mission à introduire dans le système (elle pourra être ou ne 
     * pas être clonée).
     */
    public void creerMission(Mission m) {
        //TODO À compléter !
    }
    
}
