/**
 * BE Java EE "Mission Impossible"
 * Mineure "développement web"
 * CentraleSupélec, campus de Rennes
 * 
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 * 2016
 */
package missions;

import java.util.ArrayList;
import javax.ejb.Remote;

/**
 * Interface distante pour l'EJB proposant l'API de traitement des missions.
 *
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 */
@Remote
public interface TraitementMissionsRemote {
    
    /**
     * Attribuer une mission à l'utilisateur connecté, supposé être un agent administratif.
     * @param missionID ID de la mission à attribuer.
     */
    public void attribuerMission(int missionID);
    
    /**
     * Récupération de l'ensemble des missions.
     * @return Liste de toutes les missions connues du système.
     */
    public ArrayList<Mission> getMissions();
    
    /**
     * Test de l'EJB de traitement des missions.
     * @return Résultat du test.
     */
    public String test();
    
    /**
     * Valider une mission.
     * @param missionID ID de la mission à valider.
     */
    public void valider(int missionID);
    
    /**
     * Refuser une mission.
     * @param missionID ID de la mission à refuser.
     */
    public void refuser(int missionID);
    
    /**
     * Demander le remboursement de frais d'une mission.
     * @param missionID ID de la mission dont le remboursement est demandé.
     */
    public void frais(int missionID);
    
    /**
     * Mettre en paiement le remboursement d'une mission.
     * @param missionID ID de la mission à rembourser.
     */
    public void payer(int missionID);
    
    /**
     * 
     * Récupération du login de l'utilisateur actuellement connecté.
     * @return Le login de l'utilisateur.
     */
    public String getUsername();
    
}
