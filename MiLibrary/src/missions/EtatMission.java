/**
 * BE Java EE "Mission Impossible"
 * Mineure "développement web"
 * CentraleSupélec, campus de Rennes
 * 
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 * 2016
 */
package missions;

/**
 * Énumération des états possible d'une mission au cours du workflow.
 *
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 */
public enum EtatMission {

    /**
     * La mission a été demandée par l'agent, mais pas encore attribuée à un
     * membre du personnel administratif.
     */
    DEMANDEE,
    /**
     * La demande de mission a été attribuée à un agent administratif mais n'a
     * pas encore été validée.
     */
    ATTRIBUEE,
    /**
     * La demande de mission a été validée par l'administration, qui a établi un
     * ordre de mission est en attente des justificatifs de paiement.
     */
    VALIDEE,
    /**
     * La demande de mission a été refusée par l'administration.
     */
    REFUSEE,
    /**
     * L'administration a reçu la demande de remboursement mais n'a pas encore
     * procédé à la mise en paiement.
     */
    EN_COURS,
    /**
     * L'administration a mis en paiement le remboursement des frais à l'agent.
     */
    PAYEE;
}
