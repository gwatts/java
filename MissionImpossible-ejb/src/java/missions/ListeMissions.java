/**
 * BE Java EE "Mission Impossible"
 * Mineure "développement web"
 * CentraleSupélec, campus de Rennes
 * 
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 * 2016
 */
package missions;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;

/**
 * EJB encapsulant toutes les missions connues du système.
 *
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 */
@Singleton
@LocalBean
public class ListeMissions {
    
    /**
     * Liste de toutes les missions connues du système.
     */
    private final ArrayList<Mission> missions;
    
    /**
     * Constructeur par défaut.
     */
    public ListeMissions() {
        this.missions = new ArrayList<>();
    }
    
    
    /**
     * Initialisation de la liste avec des missions de test.
     */
    @PostConstruct
    public void initialisation() {
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            this.missions.add(new Mission(EtatMission.PAYEE, "Rivet City", formatter.parse("05/01/2016"), "agent1", "admin1"));
            this.missions.add(new Mission(EtatMission.DEMANDEE, "Novac", formatter.parse("01/06/2016"), "agent1", null));
            this.missions.add(new Mission(EtatMission.ATTRIBUEE, "Underworld", formatter.parse("01/04/2016"), "agent1", "admin2"));
            this.missions.add(new Mission(EtatMission.REFUSEE, "Grayditch", formatter.parse("01/02/2016"), "agent2", "admin2"));
            this.missions.add(new Mission(EtatMission.DEMANDEE, "The Pitt", formatter.parse("01/07/2016"), "agent2", null));
            this.missions.add(new Mission(EtatMission.EN_COURS, "Gif-sur-Yvette", formatter.parse("02/03/1956"), "agent2", "admin1"));
            this.missions.add(new Mission(EtatMission.VALIDEE, "Germantown", formatter.parse("24/04/2016"), "agent3", "admin1"));
            this.missions.add(new Mission(EtatMission.PAYEE, "Megaton", formatter.parse("12/10/2015"), "agent3", "admin2"));
            this.missions.add(new Mission(EtatMission.DEMANDEE, "Arefu", formatter.parse("14/05/2016"), "agent3", null));
            
        } catch (ParseException ex) {
            Logger.getLogger(ListeMissions.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Récupération de l'ensemble des missions.
     * @return Liste de toutes les missions connues du système.
     */
    public ArrayList<Mission> getMissions() {
        return new ArrayList<>(this.missions);
        //return this.missions; //TODO restaurer ? (note à GP)
    }
    
    
    /**
     * Récupération de toutes les missions dans un état donné.
     * @param em L'état en question.
     * @return La liste de toutes les missions actuellement dans cet état.
     */
    private ArrayList<Mission> getMissionsParEtat(EtatMission em) {
        ArrayList<Mission> liste = new ArrayList<>();
        for (Mission m : this.missions) {
            if (m.getEtat() == em) {
                liste.add(m);
            }
        }
        return liste;
    }
    
    public ArrayList<Mission> getMissionsParEtatSuiviesPar(EtatMission em, String admin)
    {
        ArrayList<Mission> missionEtat = this.getMissionsParEtat(em);
        ArrayList<Mission> suiviePar = new ArrayList<Mission>();
        for (Mission mission : missionEtat)
        {
            if (mission.getAgentAdmin().equals(admin))
            {
                suiviePar.add(mission);
            }
        }
        return suiviePar;
    }
    
    public ArrayList<Mission> getMissionsParEtatFaitesPar(EtatMission em, String agent)
    {
        ArrayList<Mission> missionEtat = this.getMissionsParEtat(em);
        ArrayList<Mission> suiviePar = new ArrayList<Mission>();
        for (Mission mission : missionEtat)
        {
            if (mission.getAgent().equals(agent))
            {
                suiviePar.add(mission);
            }
        }
        return suiviePar;
    }
    
    /**
     * Recherche d'une mission par son identifiant.
     * @param id ID de la mission à rechercher.
     * @return Référence sur la mission en question, ou null en cas d'échec.
     */
    public Mission getMissionById(int id) {
        for (Mission m : this.missions) {
            if (m.getId() == id) {
                return m;
            }
        }
        return null;
    }
    
    /**
     * Récupération de toutes les missions dans l'état DEMANDEE.
     * @return Liste de toutes les missions dans l'état DEMANDEE.
     */
    public ArrayList<Mission> getMissionsDemandees() {
        return this.getMissionsParEtat(EtatMission.DEMANDEE);
    }
    
    /**
     * Récupération de toutes les missions suivies par un agent administratif donné.
     * @param admin Le login de l'agent administratif.
     * @return La liste de toutes les missions suivies par cet agent.
     */
    public ArrayList<Mission> missionsSuiviesPar(String admin) {
        ArrayList<Mission> liste = new ArrayList<>();
        for (Mission m : this.missions) {
            String agentAdmin = m.getAgentAdmin();
            if ((agentAdmin != null) && agentAdmin.equals(admin)) {
                liste.add(m);
            }
        }
        return liste;
    }
    
    /**
     * Récupération de toutes les missions demandées par un agent donné.
     * @param agent Le login de l'agent en question.
     * @return La liste de toutes les missions demandées par cet agent.
     */
    public ArrayList<Mission> missionsParAgent(String agent) {
        ArrayList<Mission> liste = new ArrayList<>();
        for (Mission m : this.missions) {
            if (m.getAgent().equals(agent)) {
                liste.add(m);
            }
        }
        return liste;
    }
    
    /**
     * Insertion d'une nouvelle mission dans le système.
     * @param m Référence sur l'objet mission à insérer.
     */
    public void ajouterMission(Mission m) {
        this.missions.add(m);
    }
}
