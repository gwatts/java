/**
 * BE Java EE "Mission Impossible"
 * Mineure "développement web"
 * CentraleSupélec, campus de Rennes
 * 
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 * 2016
 */
package agents;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;

/**
 * EJB encapsulant le compte de l'utilisateur connecté.
 *
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 */
@Stateful(name="Account")
public class Account implements AccountRemote {
    
    /**
     * Injection du contexte de session, permettant de récupérer les infos
     * d'authentification.
     */
    @Resource
    private SessionContext context;
    
    /**
     * Constructeur par défaut.
     */
    public Account() {
    }
    
    @Override
    public String getUsername() {
        return context.getCallerPrincipal().getName();
    }
}
