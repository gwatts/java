/**
 * BE Java EE "Mission Impossible"
 * Mineure "développement web"
 * CentraleSupélec, campus de Rennes
 * 
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 * 2016
 */
package client;

import java.rmi.AccessException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import missions.TraitementMissionsRemote;

/**
 * @author Guillaume Piolle <guillaume.piolle@centralesupelec.fr>
 * 
 * Classe principale du client lourd.
 * Elle récupère le "stub" d'un EJB distant et passe la main à la classe 
 * d'interface graphique.
 * 
 * Si vous arrivez à récupérer un "stub" pour l'EJB Account, faites-moi signe.
 */
public abstract class Controller {

    /**
     * Récupère un "stub" pour l'EJB TraitementMissions.
     * @return Un "stub" correspondant à l'interface TraitementMissionsRemote.
     */
    private static TraitementMissionsRemote lookupTraitementMissionsRemote() {
        try {
            Context c = new InitialContext();
            return (TraitementMissionsRemote) c.lookup("java:comp/env/TraitementMissions");
        } catch (NamingException ne) {
            System.out.println(ne);
            Throwable t = ne;
            while (t != null) {
                // On a probablement un problème de login / mot de passe,
                // ou une mauvaise configuration de la sécurité sur Glassfish.
                if (t instanceof AccessException) {
                    Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, "Erreur d'authentification, fin de l'application.");
                }
                t = t.getCause();
            }
            System.exit(1);
            return null;
        }
    }

    /**
     * Point d'entrée du client lourd.
     * @param args Arguments de la ligne de commande.
     */
    public static void main(String[] args) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        // Le "stub" de l'EJB
        final TraitementMissionsRemote tm = lookupTraitementMissionsRemote();

        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {

                // Création de la fenêtre principale
                 MainWindow mw = new MainWindow(tm);
                 mw.setTitle("Mission: Impossible - Pour une gestion moderne des déplacements professionnels");
                 mw.setVisible(true);
            }
        }
        );
    }

}
